# 中国人民大学自动完成课程作业脚本
## 1. git clone https://github.com/louislivi/autotask; 或者直接右上角点击download
## 2. 双击打开/package/dist/autotask/autotask.exe;
## 3. 在命令窗口键入 用户名，密码脚本将自动运行.
#### NOTIC:
	请求速度有所控制，防止ip被封。
	运行错误，请尝试重试，重试还是不行请等待5-10分钟后在再次尝试。
	将在下个版本增加防封ip策略。
 ![image](https://github.com/louislivi/autotask/blob/master/image/IMG_0360.JPG)
 ![image](https://github.com/louislivi/autotask/blob/master/image/IMG_0361.JPG)

